package com.pouya.kianapp.util
enum class SortOrder {
    ASCENDING, DESCENDING;
}