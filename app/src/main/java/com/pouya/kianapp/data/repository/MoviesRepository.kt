package com.pouya.kianapp.data.repository

import com.pouya.kianapp.data.datasource.remote.RemoteDataSource
import com.pouya.kianapp.domain.mapper.toMovieModel
import com.pouya.kianapp.domain.model.MovieModel
import com.pouya.kianapp.domain.movies.IMoviesRepository
import com.pouya.kianapp.extension.changeState
import com.pouya.kianapp.result.State
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject


class MoviesRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : IMoviesRepository {

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun getPopularMovies(page:Int): Flow<State<MovieModel>> {
        return flow {
            val response = remoteDataSource.getPopularMovies(page)
            val result = response.changeState { movieResponse ->
                movieResponse.toMovieModel()
            }
            emit(result)
        }.onStart {
            emit(State.Loading)
        }.flowOn(Dispatchers.IO)
    }

}
