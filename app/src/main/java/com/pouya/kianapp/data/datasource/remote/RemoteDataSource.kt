package com.pouya.kianapp.data.datasource.remote

import com.pouya.kianapp.data.datasource.remote.response.MovieResponse
import com.pouya.kianapp.data.datasource.remote.response.MovieDetailResponse
import com.pouya.kianapp.data.datasource.remote.service.TmdbService
import com.pouya.kianapp.extension.withResponse
import com.pouya.kianapp.result.State
import java.io.IOException
import java.lang.Exception
import javax.inject.Inject


class RemoteDataSource @Inject constructor(val tmdbService: TmdbService) {
    suspend fun getPopularMovies(
        page: Int): State<MovieResponse> {
        return withResponse {
            tmdbService.popularMovies(page)
        }
    }

    suspend fun movieDetail(
        movieId: Int
    ): State<MovieDetailResponse> {
        return withResponse {
            tmdbService.movieDetail(movieId)
        }
    }
}