package com.pouya.kianapp.data.datasource.remote.service

import com.pouya.kianapp.data.datasource.remote.response.MovieResponse
import com.pouya.kianapp.data.datasource.remote.response.MovieDetailResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface TmdbService {

    @Headers("Content-Type: application/json")
    @GET("movie/popular")
    suspend fun popularMovies(
        @Query("page") offset: Int,
        @Query("language") language: String = "en-US"
    ): Response<MovieResponse>

    @Headers("Content-Type: application/json")
    @GET("movie/{movie_id}")
    suspend fun movieDetail(
        @Path("movie_id") movieId: Int,
        @Query("language") language: String = "en-US"
    ): Response<MovieDetailResponse>

}