package com.pouya.kianapp.data.repository

import com.pouya.kianapp.data.datasource.remote.RemoteDataSource
import com.pouya.kianapp.domain.detail.IMovieDetailRepository
import com.pouya.kianapp.domain.mapper.toMovieDetailItemEntity
import com.pouya.kianapp.domain.model.MovieDetailModel
import com.pouya.kianapp.extension.changeState
import com.pouya.kianapp.result.State
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject


class MoviesDetailRepository @Inject constructor(
    var remoteDataSource: RemoteDataSource
) : IMovieDetailRepository {

    @ExperimentalCoroutinesApi
    @FlowPreview
    override fun getMovieDetail(movieId: Int): Flow<State<MovieDetailModel>> {
        return flow {
            val response = remoteDataSource.movieDetail(movieId)
            val result = response.changeState { movie ->
                movie.toMovieDetailItemEntity()
            }
            emit(result)
        }.onStart {
            emit(State.Loading)
        }.flowOn(Dispatchers.IO)
    }

}


