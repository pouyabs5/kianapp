package com.pouya.kianapp.data.datasource.remote.base

import retrofit2.Response

sealed class ApiResponse<T> {
    companion object {

        fun <T> create(response: Response<T>): ApiResponse<T> {
            return when {
                response.isSuccessful -> {
                    val body = response.body()
                    if (body == null || response.code() == 204) {
                        ApiEmptyResponse()
                    } else {
                        ApiSuccessResponse(body = body)
                    }
                }
                response.code() == 500 -> ApiErrorResponse(
                    Throwable(response.message() ?: "Server is unreachable!")
                )
                else -> {
                    val msg = response.errorBody()?.string()
                    val errorMsg = if (msg.isNullOrEmpty()) response.message() else msg
                    ApiErrorResponse(
                        Throwable(errorMsg ?: "unknown error")
                    )
                }
            }
        }

    }
}

class ApiEmptyResponse<T> : ApiResponse<T>()
data class ApiSuccessResponse<T>(val body: T) : ApiResponse<T>()
data class ApiErrorResponse<T>(val networkError: Throwable?) : ApiResponse<T>()
