package com.pouya.kianapp.domain.movies

import com.pouya.kianapp.di.scope.IoDispatcher
import com.pouya.kianapp.domain.base.BaseUaeCase
import com.pouya.kianapp.domain.model.MovieModel
import com.pouya.kianapp.result.State
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class MoviesUseCase @Inject constructor(
    private val repo: IMoviesRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : BaseUaeCase<Int, MovieModel>(dispatcher) {
    override fun execute(
        parameters: Int
    ): Flow<State<MovieModel>> {
        return repo.getPopularMovies(parameters)
    }
}