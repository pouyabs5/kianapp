package com.pouya.kianapp.domain.detail

import com.pouya.kianapp.domain.model.MovieDetailModel
import com.pouya.kianapp.result.State
import kotlinx.coroutines.flow.Flow

interface IMovieDetailRepository {
    fun getMovieDetail(
        movieId: Int
    ): Flow<State<MovieDetailModel>>
}