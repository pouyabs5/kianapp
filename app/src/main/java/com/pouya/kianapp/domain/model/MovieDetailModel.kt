package com.pouya.kianapp.domain.model

data class MovieDetailModel(
    val adult: Boolean? = false,
    val backdrop_path: String? = "",
    val belongs_to_collection: Any? = Any(),
    val budget: Int? = 0,
    val genres: List<Genre?>? = listOf(),
    val homepage: String? = "",
    val id: Int? = 0,
    val imdbId: String? = "",
    val original_language: String? = "",
    val original_title: String? = "",
    val overview: String? = "",
    val popularity: Double? = 0.0,
    val poster_path: String? = "",
    val release_date: String? = "",
    val revenue: Int? = 0,
    val runtime: Int? = 0,
    val status: String? = "",
    val tagLine: String? = "",
    val title: String? = "",
    val video: Boolean? = false,
    val vote_average: Double? = 0.0,
    val vote_count: Int? = 0
) {
    data class Genre(
        val id: Int? = 0,
        val name: String? = ""
    )
}