package com.pouya.kianapp.domain.movies

import com.pouya.kianapp.domain.model.MovieModel
import com.pouya.kianapp.result.State
import kotlinx.coroutines.flow.Flow

interface IMoviesRepository {
    fun getPopularMovies(page: Int): Flow<State<MovieModel>>
}