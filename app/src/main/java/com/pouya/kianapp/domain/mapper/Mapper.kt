package com.pouya.kianapp.domain.mapper

import com.pouya.kianapp.data.datasource.remote.response.MovieDetailResponse
import com.pouya.kianapp.data.datasource.remote.response.MovieResponse
import com.pouya.kianapp.domain.model.MovieDetailModel
import com.pouya.kianapp.domain.model.MovieModel

fun MovieResponse.toMovieModel() : MovieModel {
    var movieItems: MutableList<MovieModel.MovieItem> = mutableListOf()
    movies.map {
        movieItems.add(
            MovieModel.MovieItem(
                it.adult,
                it.backdropPath,
                it.id,
                it.originalTitle,
                it.overview,
                it.popularity,
                it.posterPath,
                it.releaseDate,
                it.title,
                it.video,
                it.voteAverage,
                it.voteCount
            )
        )
    }
    return MovieModel(
        page,
        movieItems,
        totalPages,
        totalResults
    )
}

fun MovieDetailResponse.toMovieDetailItemEntity() =
    MovieDetailModel(
        title = title,
        adult = adult,
        original_title = originalTitle,
        id = id,
        overview = overview,
        release_date = releaseDate,
        poster_path = posterPath,
        vote_count = voteCount,
        vote_average = voteAverage,
        backdrop_path = backDropPath
    )


