package com.pouya.kianapp.domain.model

data class MovieModel(
    val page: Int =0,
    val movieItems: List<MovieItem>,
    val total_pages: Int =0,
    val total_results: Int =0
) {
    data class MovieItem(
        val adult: Boolean?,
        val backdrop_path: String?,
        val id: Int,
        val original_title: String?,
        val overview: String?,
        val popularity: Float?,
        val poster_path: String?,
        val release_date: String,
        val title: String?,
        val video: Boolean?,
        val vote_average: Float?,
        val vote_count: Int?
    )
}
