package com.pouya.kianapp.domain.detail

import com.pouya.kianapp.di.scope.IoDispatcher
import com.pouya.kianapp.domain.base.BaseUaeCase
import com.pouya.kianapp.domain.model.MovieDetailModel
import com.pouya.kianapp.result.State
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class MovieDetailUseCase @Inject constructor(
    private val repo: IMovieDetailRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : BaseUaeCase<Int, MovieDetailModel>(dispatcher) {
    override fun execute(
        parameters: Int
    ): Flow<State<MovieDetailModel>> {
        return repo.getMovieDetail(parameters)
    }
}