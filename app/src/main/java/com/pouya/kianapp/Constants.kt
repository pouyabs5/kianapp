package com.pouya.kianapp

object Constants {
    object API {
        const val URL = "https://api.themoviedb.org/3/"
        const val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500"
    }
}