package com.pouya.kianapp.di.module

import androidx.lifecycle.ViewModel
import com.pouya.kianapp.di.scope.ViewModelKey
import com.pouya.kianapp.presentation.ui.detail.DetailViewModel
import com.pouya.kianapp.presentation.ui.movies.MoviesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MoviesViewModel::class)
    internal abstract fun bindMoviesViewModel(moviesViewModel : MoviesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    internal abstract fun bindDetailViewModel(detailViewModel: DetailViewModel): ViewModel

}