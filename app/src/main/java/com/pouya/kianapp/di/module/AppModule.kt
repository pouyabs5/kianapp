package com.pouya.kianapp.di.module

import android.app.Application
import android.content.Context
import com.pouya.kianapp.BuildConfig
import com.pouya.kianapp.Constants
import com.pouya.kianapp.data.datasource.remote.RemoteDataSource
import com.pouya.kianapp.data.datasource.remote.service.TmdbService
import com.pouya.kianapp.data.repository.MoviesDetailRepository
import com.pouya.kianapp.data.repository.MoviesRepository
import com.pouya.kianapp.domain.detail.IMovieDetailRepository
import com.pouya.kianapp.domain.movies.IMoviesRepository
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class, CoroutinesModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideTmdbService(): TmdbService {
        val client = OkHttpClient.Builder().apply {
            if (BuildConfig.DEBUG) {
                val logging = HttpLoggingInterceptor()
                logging.level = HttpLoggingInterceptor.Level.BODY
                addNetworkInterceptor(logging)
            }
        }.addInterceptor {
            val request = it.request()
            val newUrl = request.url.newBuilder()
                .addQueryParameter("api_key", "432b486d7384e9ea04ad5330ca1e8213")
                .build()
            it.proceed(
                request.newBuilder()
                    .url(newUrl)
                    .build()
            )
        }.build()
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.API.URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
        return retrofit.create(TmdbService::class.java)
    }

    @Singleton
    @Provides
    fun provideMoviesRepository(remoteDataSource: RemoteDataSource): IMoviesRepository {
        return MoviesRepository(remoteDataSource)
    }

    @Singleton
    @Provides
    fun provideMovieDetailRepository(remoteDataSource: RemoteDataSource): IMovieDetailRepository {
        return MoviesDetailRepository(remoteDataSource)
    }
}