package com.pouya.kianapp.di.module

import com.pouya.kianapp.presentation.ui.detail.DetailActivity
import com.pouya.kianapp.presentation.ui.movies.MoviesActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector
    internal abstract fun contributeMoviesActivity() : MoviesActivity

    @ContributesAndroidInjector
    internal abstract fun contributeDetailActivity() : DetailActivity
}