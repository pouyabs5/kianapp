package com.pouya.kianapp.di.module

import androidx.lifecycle.ViewModelProvider
import com.pouya.kianapp.util.ViewModelProviderFactory
import dagger.Binds
import dagger.Module

@Module
interface ViewModelFactoryModule {

    @Binds
    fun bindViewModelFactory(factory: ViewModelProviderFactory): ViewModelProvider.Factory
}