package com.pouya.kianapp.di.component

import android.app.Application
import com.pouya.kianapp.App
import com.pouya.kianapp.di.module.ActivityModule
import com.pouya.kianapp.di.module.AppModule
import com.pouya.kianapp.di.module.ViewModelFactoryModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidInjectionModule::class, AppModule::class, ActivityModule::class, ViewModelFactoryModule::class]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}