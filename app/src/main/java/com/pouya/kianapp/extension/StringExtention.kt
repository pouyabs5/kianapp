package com.pouya.kianapp.extension

import java.text.SimpleDateFormat
import java.util.*

fun String.convertDateToLong(): Long? {
    val formatter = SimpleDateFormat(
        "yyyy-MM-dd", Locale.ROOT
    )
    return formatter.parse(this)?.time
}