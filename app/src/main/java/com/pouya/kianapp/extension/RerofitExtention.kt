package com.pouya.kianapp.extension


import com.pouya.kianapp.data.datasource.remote.base.ApiEmptyResponse
import com.pouya.kianapp.data.datasource.remote.base.ApiErrorResponse
import com.pouya.kianapp.data.datasource.remote.base.ApiResponse
import com.pouya.kianapp.data.datasource.remote.base.ApiSuccessResponse
import com.pouya.kianapp.result.State
import retrofit2.Response

inline fun <T> withResponse(function: () -> Response<T>): State<T> {
    return try {
        when (val response =
            ApiResponse.create(
                function.invoke()
            )) {
            is ApiSuccessResponse -> State.Success(response.body)
            is ApiEmptyResponse -> State.Empty
            is ApiErrorResponse -> State.Error(response.networkError)
        }
    } catch (e: Exception) {
        e.printStackTrace()
        State.Error(e)
    }
}