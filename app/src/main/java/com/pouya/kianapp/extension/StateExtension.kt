package com.pouya.kianapp.extension

import com.pouya.kianapp.result.State

inline fun <T, R> State<T>.changeState(crossinline transform: (T) -> R): State<R> {
    return this.let {
        when(it){
            is State.Success -> State.Success(transform(it.data))
            is State.Error -> State.Error(it.exception)
            is State.Loading -> State.Loading
            is State.Empty -> State.Empty
        }
    }
}
