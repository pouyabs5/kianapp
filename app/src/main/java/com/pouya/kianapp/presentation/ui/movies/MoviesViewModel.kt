package com.pouya.kianapp.presentation.ui.movies

import androidx.lifecycle.*
import com.pouya.kianapp.domain.model.MovieModel
import com.pouya.kianapp.domain.movies.MoviesUseCase
import com.pouya.kianapp.extension.convertDateToLong
import com.pouya.kianapp.result.State
import com.pouya.kianapp.util.SortOrder
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class MoviesViewModel @Inject constructor(
    private val moviesUseCase: MoviesUseCase
) :
    ViewModel() {

    private var page = 1
    private var totalPages = 1
    private var totalMovies: MutableList<MovieModel.MovieItem> = mutableListOf()
    private val _listLiveData = MutableLiveData<State<MovieModel>>()
    private var _isWaitingLoadMore = false

    init {
        getData()
    }

    val moviesLiveData: LiveData<State<MovieModel>>
        get() = _listLiveData

    fun getData() {
        viewModelScope.launch {
            moviesUseCase(page).collect { movie ->
                if (movie is State.Success) {
                    nextPage()
                    totalPages = movie.data.total_pages
                    setList(movie.data.movieItems)
                } else {
                    _listLiveData.value = movie
                }
            }
        }
    }

    fun loadMore() {
        if (page <= totalPages && !_isWaitingLoadMore) {
            viewModelScope.launch {
                _isWaitingLoadMore = true
                moviesUseCase(page).collect {
                    if (it != State.Loading) {
                        _isWaitingLoadMore = false
                    }
                    if (it is State.Success) {
                        nextPage()
                        setList(it.data.movieItems)
                    } else {
                        _listLiveData.value = it
                    }
                }
            }
        }
    }

    private fun setList(movieItems: List<MovieModel.MovieItem>) {
        totalMovies.addAll(movieItems)
        _listLiveData.value =
            State.Success(MovieModel(movieItems = totalMovies))
    }


    fun setOrder(sortOrder: SortOrder) {
        val sortedList =sortList(sortOrder)
        _listLiveData.value =
            State.Success(MovieModel(movieItems = sortedList))
    }

    private fun nextPage() {
        page++
    }

    private fun sortList(sortOrder: SortOrder): MutableList<MovieModel.MovieItem> = when (sortOrder) {
        SortOrder.ASCENDING -> totalMovies.distinctBy { it.id }.sortedBy { it.release_date.convertDateToLong() }
            .toMutableList()
        SortOrder.DESCENDING -> totalMovies.distinctBy { it.id }.sortedByDescending { it.release_date.convertDateToLong() }
            .toMutableList()
    }
}