package com.pouya.kianapp.presentation.ui.base

import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.viewbinding.ViewBinding

/**
 * A generic RecyclerView adapter that uses View Binding & DiffUtil.
 *  <T> Type of the items in the list
 *  <V> The type of the ViewBinding
 */
abstract class BaseListAdapter<T, V : ViewBinding>(
    diffCallback: DiffUtil.ItemCallback<T>
) : ListAdapter<T, BaseViewHolder<V>>(
    AsyncDifferConfig.Builder<T>(diffCallback)
        .build()
) {

    var onItemClickListener: OnListItemClickListener? = null

    fun setOnListItemClickListener(listener: OnListItemClickListener) {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<V> {
        val binding = createBinding(parent)
        return BaseViewHolder(binding)
    }

    protected abstract fun createBinding(parent: ViewGroup): V

    override fun onBindViewHolder(holder: BaseViewHolder<V>, position: Int) {
        bind(holder.binding, getItem(position))
    }

    protected abstract fun bind(binding: V, item: T)
}

interface OnListItemClickListener {

    fun <T> onItemClicked(data: T)

}
