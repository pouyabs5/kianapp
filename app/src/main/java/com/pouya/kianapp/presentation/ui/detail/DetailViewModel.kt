package com.pouya.kianapp.presentation.ui.detail

import androidx.lifecycle.*
import com.pouya.kianapp.domain.detail.MovieDetailUseCase
import com.pouya.kianapp.domain.model.MovieDetailModel
import com.pouya.kianapp.result.State
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class DetailViewModel @Inject constructor(
    private val movieUseCase: MovieDetailUseCase
) :
    ViewModel() {

    private val _movieLiveData = MutableLiveData<State<MovieDetailModel>>()
    val movieLiveData: LiveData<State<MovieDetailModel>>
        get() = _movieLiveData

    fun detail(id:Int) {
        viewModelScope.launch {
            movieUseCase(id).collect {
                _movieLiveData.value = it
            }
        }
    }



}