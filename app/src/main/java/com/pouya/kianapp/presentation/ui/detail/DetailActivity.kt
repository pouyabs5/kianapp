package com.pouya.kianapp.presentation.ui.detail

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.pouya.kianapp.Constants
import com.pouya.kianapp.R
import com.pouya.kianapp.databinding.ActivityDetailBinding
import com.pouya.kianapp.domain.model.MovieDetailModel
import com.pouya.kianapp.extension.viewModelOf
import com.pouya.kianapp.presentation.ui.base.BaseActivity
import com.pouya.kianapp.result.State
import com.squareup.picasso.Picasso
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.IOException

@ExperimentalCoroutinesApi
class DetailActivity : BaseActivity<DetailViewModel, ActivityDetailBinding>() {

    private var idMovie: Int? = null

    override fun getViewBinding(): ActivityDetailBinding =
        ActivityDetailBinding.inflate(layoutInflater)

    override fun getViewModel() = viewModelOf<DetailViewModel>(mViewModelProvider)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
         idMovie = intent.getBundleExtra("data")?.getInt("id")
        initUI()
        getData()
        initViewModel()

    }

    private fun initUI() {
        mViewBinding.emptyStateButton.setOnClickListener {
            goneView()
            showProgress()
            getData()
        }
    }

    private fun getData(){
        idMovie?.let {
            mViewModel.detail(it)
        }
    }

    private fun initViewModel() {
        mViewModel.movieLiveData.observe(this, Observer {
            when (it) {
                is State.Success -> {
                    initData(it.data)
                }
                is State.Error -> {
                    hideProgress()
                    if (it.exception is IOException) {
                        showError(getString(R.string.no_internet_access))
                    } else {
                        showError(it.exception.toString())
                    }
                }
                is State.Loading -> {
                    showProgress()
                }
                is State.Empty -> {

                }
            }
        })
    }

    private fun initData(data: MovieDetailModel) {
        mViewBinding.constraintMain.visibility = View.VISIBLE
        hideProgress()
        mViewBinding.tvSubTitle.text = data.original_title
        mViewBinding.toolbar.title = data.title
        mViewBinding.tvTipDescription.text = data.overview
        mViewBinding.tvReleaseDate.text = data.release_date
        mViewBinding.tvRate.also {
            it.text = data.vote_average.toString()
            it.setBackgroundColor(Color.parseColor("#${Color.GRAY}"))
        }
        Picasso.get().load("${Constants.API.IMAGE_BASE_URL}${data.backdrop_path}").into(mViewBinding.ivMovie)
        Picasso.get().load("${Constants.API.IMAGE_BASE_URL}${data.poster_path}").into(mViewBinding.ivDescription)

    }

    private fun showError(showError: String?) {
        mViewBinding.viewState.visibility = View.VISIBLE
        mViewBinding.emptyStateTv.text = showError ?: getString(R.string.no_results_found)
    }

    private fun goneView() {
        mViewBinding.viewState.visibility = View.GONE
        mViewBinding.constraintMain.visibility = View.GONE
    }

    private fun hideProgress() {
        mViewBinding.progressBar.visibility = View.GONE
    }

    private fun showProgress() {
        mViewBinding.progressBar.visibility = View.VISIBLE
    }

}