package com.pouya.kianapp.presentation.ui.movies.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.pouya.kianapp.Constants
import com.pouya.kianapp.databinding.ItemMovieBinding
import com.pouya.kianapp.domain.model.MovieModel
import com.pouya.kianapp.presentation.ui.base.BaseListAdapter
import com.squareup.picasso.Picasso

class MovieListAdapter : BaseListAdapter<MovieModel.MovieItem, ItemMovieBinding>(
    object : DiffUtil.ItemCallback<MovieModel.MovieItem>() {
        override fun areItemsTheSame(
            new: MovieModel.MovieItem,
            old: MovieModel.MovieItem
        ): Boolean {
            return new.id == old.id
        }

        override fun areContentsTheSame(
            new: MovieModel.MovieItem,
            old: MovieModel.MovieItem
        ): Boolean {
            return new == old
        }
    }
) {

    override fun createBinding(parent: ViewGroup): ItemMovieBinding {
        return ItemMovieBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    }

    override fun bind(binding: ItemMovieBinding, item: MovieModel.MovieItem) {

        binding.tvTitle.text = item.title
        binding.tvDescription.text = item.release_date
        binding.tvRate.text = item.vote_average.toString()

        binding.root.setOnClickListener {
            onItemClickListener?.apply {
                onItemClicked(item)
            }
        }

        Picasso.get().load("${Constants.API.IMAGE_BASE_URL}${item.poster_path}").into(binding.ivMovie)


    }


}
