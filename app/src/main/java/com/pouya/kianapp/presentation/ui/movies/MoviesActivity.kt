package com.pouya.kianapp.presentation.ui.movies

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pouya.kianapp.R
import com.pouya.kianapp.databinding.ActivityMoviesBinding
import com.pouya.kianapp.domain.model.MovieModel
import com.pouya.kianapp.extension.viewModelOf
import com.pouya.kianapp.presentation.ui.base.BaseActivity
import com.pouya.kianapp.presentation.ui.base.OnListItemClickListener
import com.pouya.kianapp.presentation.ui.detail.DetailActivity
import com.pouya.kianapp.presentation.ui.movies.adapter.MovieListAdapter
import com.pouya.kianapp.result.State
import com.pouya.kianapp.util.SortOrder
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.IOException


@ExperimentalCoroutinesApi
class MoviesActivity :
    BaseActivity<MoviesViewModel, ActivityMoviesBinding>(),
    OnListItemClickListener {

    override fun getViewBinding(): ActivityMoviesBinding =
        ActivityMoviesBinding.inflate(layoutInflater)

    override fun getViewModel() = viewModelOf<MoviesViewModel>(mViewModelProvider)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        initViewModel()
        initUi()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.filter_newer -> {
                adapter.submitList(null)
                mViewModel.setOrder(SortOrder.DESCENDING)
                true
            }
            R.id.filter_older -> {
                adapter.submitList(null)
                mViewModel.setOrder(SortOrder.ASCENDING)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initViewModel() {
        mViewModel.moviesLiveData.observe(this, Observer {
            when (it) {
                is State.Success -> {
                    hideProgress {
                        if (it.data.movieItems.isEmpty()) {
                            showEmptyState(null)
                        }
                    }
                    if (it.data.movieItems.isNotEmpty()) {
                        adapter.submitList(it.data.movieItems)
                        showLoadMoreLoading(false)
                    }
                }
                is State.Error -> {
                    if (getListCount() == 0) {
                        hideProgress {
                            if (it.exception is IOException) {
                                showEmptyState(getString(R.string.no_internet_access))
                            } else {
                                showEmptyState(it.exception.toString())
                            }
                        }
                    } else {
                        showLoadMoreLoading(false)
                        Toast.makeText(this, it.exception.toString(), Toast.LENGTH_SHORT).show()
                    }
                }
                is State.Loading -> {
                    if (getListCount() == 0) {
                        hideEmptyState()
                        showProgress()
                    } else {
                        showLoadMoreLoading(true)
                    }
                }
                is State.Empty -> {
                    if (getListCount() == 0) {
                        showEmptyState(null)
                    }
                }
            }
        })

    }

    private fun getListCount(): Int = adapter.itemCount

    private fun initUi() {
        initRecyclerView()
        setSupportActionBar(mViewBinding.toolbar)
        mViewBinding.emptyStateButton.setOnClickListener {
            mViewBinding.viewState.visibility = View.GONE
            mViewModel.getData()

        }
    }

    private fun initRecyclerView() {
        mViewBinding.recyclerView.layoutManager = LinearLayoutManager(this)
        mViewBinding.recyclerView.adapter = adapter
        mViewBinding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(view: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(view, dx, dy)
                if (dy > 0) {
                    val layoutManager = mViewBinding.recyclerView.layoutManager as LinearLayoutManager
                    val lastPosition = layoutManager.findLastVisibleItemPosition()
                    if (lastPosition == adapter.itemCount - 1) {
                        mViewModel.loadMore()
                    }
                }
            }
        })
    }

    private val adapter: MovieListAdapter by lazy {
        MovieListAdapter().also {
            it.setOnListItemClickListener(this)
        }
    }


    private fun showProgress() {
        mViewBinding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgress(endAction: () -> Unit) {
        mViewBinding.progressBar.visibility = View.GONE
        endAction()
    }

    private fun showEmptyState(showError: String?) {
        mViewBinding.viewState.visibility = View.VISIBLE
        mViewBinding.emptyStateTv.text = showError ?: getString(R.string.no_results_found)
    }


    private fun hideEmptyState() {
        mViewBinding.viewState.visibility = View.GONE
    }

    private fun showLoadMoreLoading(visible: Boolean) = when {
        visible -> {
            mViewBinding.loadMore.visibility = View.VISIBLE
        }
        else -> {
            mViewBinding.loadMore.visibility = View.GONE
        }
    }

    override fun <T> onItemClicked(data: T) {
        val intent = Intent(this, DetailActivity::class.java)
        val bundle = Bundle().apply {
            this.putInt("id", (data as MovieModel.MovieItem).id)
        }
        intent.putExtra("data", bundle)
        startActivity(intent)
    }

}