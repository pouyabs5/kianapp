package com.pouya.kianapp.result

/**
 * State Management for UI & Data.
 */
sealed class State<out T> {
    data class Success<out T>(val data: T) : State<T>()
    data class Error(val exception: Throwable?) : State<Nothing>()
    object Loading : State<Nothing>()
    object Empty : State<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
            Loading -> "Loading"
            Empty -> "Empty"
        }
    }
}


val <T> State<T>.data: T?
    get() = (this as? State.Success)?.data

